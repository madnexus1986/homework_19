// Homework_19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Animal
{
public:
    Animal()
    {}
    virtual void Voice() 
    {}
};

class Dog : public Animal
{
public:
    Dog()
    {}
    void Voice() override
    {
        std::cout << "Woof-woof\n";
    }
};

class Cat : public Animal
{
public:
    Cat()
    {}
    void Voice() override
    {
        std::cout << "Myau\n";
    }
};

class Cow : public Animal
{
public:
    Cow()
    {}
    void Voice() override
    {
        std::cout << "Mooooo\n";
    }
};


int main()
{
    Animal* animalArray[3];

    animalArray[0] = new Dog();
    animalArray[1] = new Cat();
    animalArray[2] = new Cow();
    std::cout << "Array with objects of child classes created\n";
    std::cout << "Printing sounds of different animals...\n";
    for (int i=0; i < 3; i++)
    {
        animalArray[i]->Voice();
    }
    

}
